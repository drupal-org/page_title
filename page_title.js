
(function ($) {

Drupal.behaviors.pageTitleFieldsetSummaries = {
  attach: function (context) {
    $('fieldset#edit-page-title', context).setSummary(function (context) {
      var pt = $('input', context).val();

      return pt ?
        Drupal.t('Page Title: @pt', { '@pt': pt }) :
        Drupal.t('No Page Title');
    });
  }
};

})(jQuery);

