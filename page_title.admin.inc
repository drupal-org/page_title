<?php

/**
 * @file
 * Admin include file.
 */

/**
 * Displays the form for the standard settings tab.
 *
 * @return
 *   array A structured array for use with Forms API.
 */
function page_title_admin_settings() {
  // Define a default looking 'form element' for setting.
  $showfield_form_element = array('#type' => 'checkbox', );


  // Define a default looking 'form element' for setting.
  $pattern_form_element = array(
    '#type' => 'textfield',
    '#size' => 30,
    '#maxlength' => 256,
  );


  // Define the basic scope column values
  $form['patterns']['scope'] = array(
    'page_title_default' => array('#markup' => t('Global Only'),),
    'page_title_front'   => array('#markup' => t('Global Only'),),
    'page_title_user'    => array('#markup' => t('User'),),
  );

  // Define the 'default' token patterns
  $form['patterns']['pattern'] = array(
    'page_title_default' => array(
      '#title' => t('Default'),
      '#default_value' => variable_get('page_title_default', '[site:page-title] | [site:name]'),
      '#required' => TRUE,
    ) + $pattern_form_element,
    'page_title_front' => array(
      '#title' => t('Frontpage'),
      '#default_value' => variable_get('page_title_front', '[site:name] | [site:slogan]'),
    ) + $pattern_form_element,
    'page_title_user' => array(
      '#title' => t('User Profile'),
      '#default_value' => variable_get('page_title_user', ''),
    ) + $pattern_form_element,
  );


  // Define the "showfield" checkbox for the user profile page
  $form['patterns']['showfield']['page_title_user_showfield'] = array(
    '#default_value' => variable_get('page_title_user_showfield', 0),
  ) + $showfield_form_element;


  // Definate the patterns per-node-type
  $types = node_type_get_types();
  foreach ($types as $type) {
    // Define the node-type key
    $key = 'page_title_type_'. $type->type;

    // Pattern entry
    $form['patterns']['pattern'][$key] = array(
      '#title' => t('Content Type - %type', array('%type' => $type->name)),
      '#default_value' => variable_get($key, ''),
    ) + $pattern_form_element;

    $form['patterns']['showfield'][$key .'_showfield'] = array(
      '#default_value' => variable_get($key .'_showfield', 0),
    ) + $showfield_form_element;

    $form['patterns']['scope'][$key] = array('#markup' => t('Node'),);
  }


  // Definate the patterns per-vocab-type - if Taxonomy Module is enabled
  if (module_exists('taxonomy')) {
    $vocabs = taxonomy_get_vocabularies();
    foreach ($vocabs as $vocab) {
      // Define the vocab key
      $key = 'page_title_vocab_'. $vocab->vid;

      // Pattern entry
      $form['patterns']['pattern'][$key] = array(
        '#title' => t('Vocabulary - %vocab_name', array('%vocab_name' => $vocab->name)),
        '#default_value' => variable_get($key, ''),
      ) + $pattern_form_element;

      $form['patterns']['showfield'][$key .'_showfield'] = array(
        '#default_value' => variable_get($key .'_showfield', 0),
      ) + $showfield_form_element;

      $form['patterns']['scope'][$key] = array('#markup' => t('Taxonomy'),);
    }
  }

  // Add the blog homepage pattern field
  if (module_exists('blog')) {
    $key = 'page_title_blog';
    $form['patterns']['pattern'][$key] = array(
      '#title' => t('Blog Homepage'),
      '#default_value' => variable_get($key, ''),
    ) + $pattern_form_element;

    $form['patterns']['scope'][$key] = array('#markup' => t('User'),);
  }

  // Define the page pattern text field. This is appended to any page requests containing 'page=[0-9]+' in the query string
  $form['page_title_pager_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Pattern for pages that contain a pager'),
    '#default_value' => variable_get('page_title_pager_pattern', ''),
    '#description' => t('This pattern will be appended to a page title for any given page with a pager on it'),
  );

  // Add the token help to a collapsed fieldset at the end of the configuration page.
  $form['token_help'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available Tokens List'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['token_help']['content'] = array(
    '#markup' => theme('page_title_token_help'),
  );


  $form = system_settings_form($form);
  unset($form['#theme']);
  return $form;
}


function theme_page_title_admin_settings($variables) {
  $form = $variables['form'];
  $rows = array();
  foreach (element_children($form['patterns']['pattern']) as $key) {
    $title = array(
      '#markup' => $form['patterns']['pattern'][$key]['#title'],
      '#required' => $form['patterns']['pattern'][$key]['#required'],
    );
    unset($form['patterns']['pattern'][$key]['#title']);
    $row = array(
      render($title),
      render($form['patterns']['scope'][$key]),
      render($form['patterns']['pattern'][$key]),
      isset($form['patterns']['showfield'][$key .'_showfield']) ? render($form['patterns']['showfield'][$key .'_showfield']) : '',
    );
    $rows[] = $row;
  }

  $output  = theme('table', array('header' => array(t('Page Type'), t('Token Scope'), t('Pattern'), t('Show Field')), 'rows' => $rows));
  $output .= drupal_render_children($form);
  return $output;
}


/**
 * An internal theme function to render the tokens to help the user. NOTE: Why is this not a core theme function?!
 */
function theme_page_title_token_help() {
  $token_info = token_info();

  $output = '<p>'. t('Available tokens are:') .'</p>';
  $output .= '<dl>';
  // TODO: Sort out user:user token help... we cant do nested tokens this way. Recursive token_help theme function needed?
  foreach(array('node', 'term', 'vocabulary', 'site', 'date') as $key) {
    $output .= "<dt><strong>{$token_info['types'][$key]['name']}</strong> - {$token_info['types'][$key]['description']}</dt>";
    $token_pairs = array();
    foreach ($token_info['tokens'][$key] as $token => $info) {
      $token_pairs[] = "<code>[{$key}:{$token}]</code> - {$info['name']}";
    }
    $output .= '<dd>'. theme('item_list', array('items' => $token_pairs)) .'</dd>';
  }
  $output .= '</dl>';
  return $output;
}
